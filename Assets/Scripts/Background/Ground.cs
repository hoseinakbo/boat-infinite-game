﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {
    
    float endLocationX = -3.83f;
    float startLocationX = 9.42f;
    Vector3 startLocation;

    GroundManager groundManager;

    SpriteRenderer spriteRenderer;

    public bool isDebugging;

	void Start () {
        groundManager = GetComponentInParent<GroundManager>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        startLocation = new Vector3(startLocationX, transform.localPosition.y, transform.localPosition.z);
	}
	
	void Update () {

        MoveGround();

        if (isDebugging)
        {
            Vector3 pos = GameManager.instance.testSphere.position;
            pos.x = transform.position.x + GetComponent<SpriteRenderer>().bounds.extents.x;
            GameManager.instance.testSphere.position = pos;
        }

        if(transform.position.x + spriteRenderer.bounds.extents.x < GameManager.instance.cameraInfo.CameraBounds.xMax)
            transform.localPosition = startLocation;
        /*
        if (transform.localPosition.x <= endLocationX)
            transform.localPosition = startLocation;*/
	}

    void MoveGround()
    {
        transform.position += transform.right * (-1) * Time.deltaTime * groundManager.speed;
    }
}