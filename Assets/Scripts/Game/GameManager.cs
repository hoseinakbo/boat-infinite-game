﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Net;

public class GameManager : MonoBehaviour
{

    static GameManager _instance;
    public static GameManager instance
    {
        get
        {
            if (!_instance)
                _instance = Object.FindObjectOfType(typeof(GameManager)) as GameManager;
            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
    }

    public CameraInfo cameraInfo;

    public Transform testSphere;
}